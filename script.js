let initPositionX = 0;
let initPositionY = 0;
let containerWidth = 0;
let containerHeight = 0;
let containerElement = null;
let innerElement = null;
const initPadding = 1;

function onDrag(event) {
    event.preventDefault()
    const clientX = event.targetTouches?.[0].clientX ?? event.x;
    const clientY = event.targetTouches?.[0].clientY ?? event.y;
    if (clientX === 0 || clientY === 0) return;
    let marginX = clientX + initPositionX;
    let marginY = clientY + initPositionY;
    if (marginX < - initPadding) {
        marginX = - initPadding;
    }
    if (marginY < - initPadding) {
        marginY = - initPadding;
    }
    const maxMarginX = containerWidth - event.target.clientWidth - initPadding;
    const maxMarginY = containerHeight - event.target.clientHeight - initPadding;
    if (marginX > maxMarginX) {
        marginX = maxMarginX;
    }
    if (marginY > maxMarginY) {
        marginY = maxMarginY;
    }
    event.target.style.marginLeft = marginX + 'px';
    event.target.style.marginTop = marginY + 'px';
}

function onDragStart(event) {
    const clientX = event.targetTouches?.[0].clientX ?? event.x;
    const clientY = event.targetTouches?.[0].clientY ?? event.y;
    containerWidth = containerElement.clientWidth;
    containerHeight = containerElement.clientHeight;
    const initMarginX = parseInt(event.target.style.marginLeft.slice(0,-2)) || 0;
    const initMarginY = parseInt(event.target.style.marginTop.slice(0,-2)) || 0;
    initPositionX = - clientX + initMarginX;
    initPositionY = - clientY + initMarginY;
    if (!event.targetTouches) {
        event.target.style.opacity = '0';
    }
}

function onDragOver(event) {
    if (!event.targetTouches) {
        event.target.style.opacity = '1';
    }
    event.preventDefault()
}

window.onload = function() {
    containerElement = document.querySelector('.container');
    containerElement.style.padding = `${initPadding}px`;
    innerElement = document.querySelector('.inner-element');
    innerElement.addEventListener('touchmove', (event) => onDrag(event));
    innerElement.addEventListener('touchstart', (event) => onDragStart(event));
    containerElement.addEventListener('touchend', (event) => onDragOver(event));
}